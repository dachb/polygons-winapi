#include "stdafx.h"
#include "DataStructures.h"
#include <forward_list>
#include "Primitives.h"

SegmentNode::SegmentNode(SegmentNode* previous, const Segment& segment, SegmentNode* next)
{
	this->previous = previous;
	current = new Segment(segment);
	this->next = next;
}

SegmentNode& SegmentNode::operator=(SegmentNode other)
{
	std::swap(this->previous, other.previous);
	std::swap(this->current, other.current);
	std::swap(this->next, other.next);
	return *this;
}

SegmentNode::~SegmentNode()
{
	delete current;
}

void SegmentNode::Reverse()
{
	auto reversed_segment = new Segment(this->current->end, this->current->start);
	std::swap(this->previous, this->next);
	delete this->current;
	this->current = reversed_segment;
}

VertexNode::VertexNode(VertexNode* previous, const Vertex& vertex, VertexNode* next, SegmentNode* in_segment, SegmentNode* out_segment)
{
	this->previous = previous;
	this->current = new Vertex(vertex);
	this->next = next;
	this->in_segment = in_segment;
	this->out_segment = out_segment;
}

VertexNode& VertexNode::operator=(VertexNode other)
{
	std::swap(this->previous, other.previous);
	std::swap(this->current, other.current);
	std::swap(this->next, other.next);
	std::swap(this->in_segment, other.in_segment);
	std::swap(this->out_segment, other.out_segment);
	return *this;
}

VertexNode::~VertexNode()
{
	delete current;
}

void VertexNode::Reverse()
{
	std::swap(this->previous, this->next);
	std::swap(this->in_segment, this->out_segment);
}

SegmentVertexList::SegmentVertexList(const SegmentVertexList& list)
{
	auto it = list.FirstVertex();
	for (auto i = 0; i < list.vertex_count + 1; ++i, it = it->next)
	{
		this->AddVertex(*it->current);
	}
	this->ClosePolygon();
}


SegmentVertexList::~SegmentVertexList()
{
	SegmentNode* prev_segment;
	auto segment_it = segment_head;
	for (auto i = 0; i < segment_count; ++i)
	{
		prev_segment = segment_it;
		segment_it = segment_it->next;
		delete prev_segment;
	}

	VertexNode* prev_vertex;
	auto vertex_it = vertex_head;
	for (auto i = 0; i < vertex_count; ++i)
	{
		prev_vertex = vertex_it;
		vertex_it = vertex_it->next;
		delete prev_vertex;
	}
}

void SegmentVertexList::AddVertex(const Vertex& vertex)
{
	auto vertex_node = new VertexNode(vertex);
	// Base case: Empty list
	if (vertex_head == nullptr)
	{
		vertex_head = vertex_tail = vertex_node;
	} 
	else
	{
		// Append last element to new tail and update tail
		vertex_node->previous = vertex_tail;
		vertex_tail->next = vertex_node;
		vertex_tail = vertex_node;
	}
	vertex_count++;

	// This condition is equivalent to having at most one vertex
	if (vertex_head == vertex_tail) return;

	Segment s(*vertex_tail->previous->current, *vertex_tail->current);
	auto segment_node = new SegmentNode(s);
	// Adding segment references
	vertex_tail->previous->out_segment = vertex_tail->in_segment = segment_node;
	// Base case: Empty list
	if (segment_head == nullptr)
	{
		segment_head = segment_tail = segment_node;
	}
	else
	{
		// Append last element to new tail and update tail
		segment_node->previous = segment_tail;
		segment_tail->next = segment_node;
		segment_tail = segment_node;
	}
	segment_count++;
}

void SegmentVertexList::ClosePolygon()
{
	// Save old tail for deletion
	auto old_segment_tail = segment_tail;
	auto old_segment = *old_segment_tail->current;
	auto& first_vertex = *vertex_head->current;
	Segment new_segment(old_segment.start, first_vertex);
	auto new_segment_tail = new SegmentNode(old_segment_tail->previous, new_segment, segment_head);
	// Replace the last segment in-place
	old_segment_tail->previous->next = new_segment_tail;
	segment_tail = new_segment_tail;
	segment_head->previous = segment_tail;
	delete old_segment_tail;
	// Remove the last vertex, which was a "fake" (it was sufficiently close to the last vertex to warrant closure)
	auto old_vertex_tail = vertex_tail;
	vertex_tail = vertex_tail->previous;
	delete old_vertex_tail;
	// Add cyclic references
	vertex_tail->next = vertex_head;
	vertex_head->previous = vertex_tail;
	vertex_tail->out_segment = segment_tail;
	vertex_head->in_segment = segment_tail;
	vertex_count--;
}

VertexNode* SegmentVertexList::FindVertex(const int& x, const int& y)
{
	auto it = vertex_tail;
	for (auto i = vertex_count; i > 0; --i, it = it->previous)
	{
		if (it->current->CheckHit(x, y))
		{
			return it;
		}
	}
	return nullptr;
}

SegmentNode* SegmentVertexList::FindSegment(const int& x, const int& y)
{
	auto it = segment_tail;
	for (auto i = segment_count; i > 0; --i, it = it->previous)
	{
		if (it->current->CheckHit(x, y))
		{
			return it;
		}
	}
	return nullptr;
}

std::list<VertexNode*> SegmentVertexList::VerticesToList() const
{
	std::list<VertexNode*> list;
	auto it = vertex_head;
	for (auto i = 0; i < vertex_count; ++i, it = it->next)
	{
		list.push_back(it);
	}
	return list;
}

void SegmentVertexList::Reverse()
{
	auto vertex_it = vertex_head;
	for (auto i = 0; i < vertex_count; ++i, vertex_it = vertex_it->next)
		vertex_it->Reverse();

	auto segment_it = segment_head;
	for (auto i = 0; i < segment_count; ++i, segment_it = segment_it->next)
		segment_it->Reverse();

	std::swap(vertex_head, vertex_tail);
	std::swap(segment_head, segment_tail);
}

void SegmentVertexList::RemoveVertex(VertexNode* vertex_node)
{
	if (vertex_node == nullptr) return;
	// Get adjacent vertices
	auto& prev_vertex = *vertex_node->previous->current;
	auto& next_vertex = *vertex_node->next->current;
	Segment new_segment(prev_vertex, next_vertex);
	auto segment_node = new SegmentNode(vertex_node->previous->in_segment, new_segment, vertex_node->next->out_segment);
	// Vertex-vertex references
	vertex_node->previous->next = vertex_node->next;
	vertex_node->next->previous = vertex_node->previous;
	// Vertex-segment references
	vertex_node->previous->out_segment = segment_node;
	vertex_node->next->in_segment = segment_node;
	// Segment-segment references
	vertex_node->previous->in_segment->next = segment_node;
	vertex_node->next->out_segment->previous = segment_node;
	// Update vertex head/tail if it is to be removed
	if (vertex_node == vertex_head) vertex_head = vertex_head->next;
	if (vertex_node == vertex_tail) vertex_tail = vertex_tail->previous;
	delete vertex_node->in_segment;
	delete vertex_node->out_segment;
	delete vertex_node;
	// Update segment head/tail
	segment_head = vertex_head->out_segment;
	segment_tail = vertex_head->in_segment;
	vertex_count--;
	segment_count--;
}

VertexNode* SegmentVertexList::BisectSegment(SegmentNode* segment_node, Vertex point)
{
	auto vertex_node = FindVertex(segment_node->current->start.x, segment_node->current->start.y);
	auto new_vertex = new VertexNode(vertex_node, point, vertex_node->next, nullptr, nullptr);
	// Segment-segment references
	auto first_segment = new SegmentNode(segment_node->previous, Segment(*new_vertex->previous->current, *new_vertex->current), nullptr);
	auto second_segment = new SegmentNode(nullptr, Segment(*new_vertex->current, *new_vertex->next->current), segment_node->next);
	first_segment->next = second_segment;
	second_segment->previous = first_segment;
	segment_node->previous->next = first_segment;
	segment_node->next->previous = second_segment;
	// Segment-vertex references
	vertex_node->out_segment = first_segment;
	new_vertex->in_segment = first_segment;
	new_vertex->out_segment = second_segment;
	vertex_node->next->in_segment = second_segment;
	// Vertex-vertex references
	vertex_node->next->previous = new_vertex;
	vertex_node->next = new_vertex;
	// Update segment head/tail if it is to be removed
	if (segment_head == segment_node) segment_head = first_segment;
	if (segment_tail == segment_node) segment_tail = second_segment;
	delete segment_node;
	vertex_count++;
	segment_count++;
	return new_vertex;
}

void SegmentVertexList::BisectSegment(SegmentNode* segment_node)
{
	auto midpoint((segment_node->current->start + segment_node->current->end) / 2);
	BisectSegment(segment_node, midpoint);
}
