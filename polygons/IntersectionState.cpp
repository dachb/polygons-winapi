﻿#include "stdafx.h"
#include "States.h"
#include <algorithm>

IntersectionState::IntersectionState(Poly* first, Poly* second)
{
	this->first = first;
	this->second = second;
}

void IntersectionState::MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y)
{
	if (!first->CheckHit(x, y) && !second->CheckHit(x, y))
	{
		first->SetSelection(false);
		second->SetSelection(false);
		isDone = true;
		nextState = new IdleState();
		return;
	}
	if (button != RIGHT_BUTTON) return;
	auto hMenu = CreatePopupMenu();
	POINT point;
	point.x = x;
	point.y = y;
	ClientToScreen(canvas_data.hWnd, &point);
	InsertMenu(hMenu, -1, MF_BYPOSITION | MF_STRING, INTERSECT, T_INTERSECT);
	TrackPopupMenu(hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, canvas_data.hWnd, nullptr);
}

void IntersectionState::Command(const CanvasData& canvas_data, const int& command)
{
	first->SetSelection(false);
	second->SetSelection(false);
	if (command == INTERSECT) {
		auto intersections = first->Intersect(*second);
		if (intersections.size() > 0) {
			canvas_data.polygons.insert(canvas_data.polygons.end(), intersections.begin(), intersections.end());
			canvas_data.polygons.erase(remove(canvas_data.polygons.begin(), canvas_data.polygons.end(), first));
			canvas_data.polygons.erase(remove(canvas_data.polygons.begin(), canvas_data.polygons.end(), second));
			delete first;
			delete second;
		}
	}
	isDone = true;
	nextState = new IdleState();
}
