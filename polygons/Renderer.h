﻿#pragma once
#include "Primitives.h"
#include "DataStructures.h"
#include <vector>

#define COLOR(r,g,b) ((COLORREF)(((BYTE)(b)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(r))<<16)))
#define RGB2BGR(rgb) (COLOR(BLUE(rgb), GREEN(rgb), RED(rgb)));
#define RED(color) (uint8_t)((color >> 16) & 0xff)
#define GREEN(color) (uint8_t)((color >> 8) & 0xff)
#define BLUE(color) (uint8_t)(color & 0xff)

Point3D ScreenPlane(int x, int y, int w, int h);
Point3D Paraboloid(int x, int y, int w, int h);
Point3D AffinePlane(int x, int y, int w, int h);
Point3D LogarithmicFunction(int x, int y, int w, int h);
Point3D Pyramid(int x, int y, int w, int h);
Point3D CutPyramid(int x, int y, int w, int h);
Point3D Cone(int x, int y, int w, int h);
Point3D Sphere(int x, int y, int w, int h);
Point3D Ripple(int x, int y, int w, int h);
Point3D Bumps(int x, int y, int w, int h);

enum LightMovementDirection
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	TOWARD_SCREEN,
	AWAY_FROM_SCREEN
};

class Renderer
{
public:
	int width, height;
	HDC hBitmapDC;

	Renderer(const int& width, const int& height);
	Renderer(const Renderer& renderer) = delete;
	Renderer& operator=(const Renderer& renderer) = delete;
	~Renderer();
	void Resized(const int& width, const int& height);
	void Draw(std::vector<Poly*> polygons);
	void SetBackground(const HBITMAP& hBitmap);
	void LoadBumpMap(const HBITMAP& hBitmap);
	void SetLightColor(const COLORREF& color);
	void SetFunction(Point3D(*f)(int, int, int, int));
	void CalculateSurfaceNormals();
	void RotateLight();
	bool ToggleUniformLight();
	void MoveLight(LightMovementDirection direction);
private:
	const int increment = 50;

	int tile_height, tile_width;
	int bump_height, bump_width;

	void CreateBitmap(const int& width, const int& height, HDC& hdc, HBITMAP& hBitmap, HBITMAP& hOldBitmap, uint32_t*& pixels);
	void CleanupBitmap(HDC& hdc, HBITMAP& hBitmap, HBITMAP& hOldBitmap);
	uint32_t *pixels;
	uint32_t *tile_pixels;
	uint32_t light_color = COLOR(255, 255, 255);
	bool is_uniform = true;
	int angle = 0;
	int light_radius;
	Point3D light_center;
	Point3D light_source;
	Point3D* height_normals;
	Point3D* surface_normals;
	Point3D(*surface_normal_function)(int, int, int, int) = Cone;
	HBITMAP hBitmap, hOldBitmap;
	HBITMAP hTile, hOldTile;
	HDC hTileDC;

	void SetPixel(const int& x, const int& y, const int& color);

	void DrawBackground();
	void Draw(const Poly& polygon);
	void Fill(const Poly& polygon);
	void ProcessNeighbors(VertexNode* vertex_node, std::vector<ActiveEdge>& active_edge_table);
	void CheckEdge(const Vertex* from, const Vertex* to, std::vector<ActiveEdge>& active_edge_table);
	void Draw(const SegmentVertexList& list, const uint32_t& color);
	void Draw(const Vertex& vertex, const uint32_t& color);
	void Draw(const Segment& segment, const uint32_t& color);

	void BresenhamLine(int x0, int y0, int x1, int y1, bool steep, const uint32_t& color);
	uint32_t LambertianReflection(int x, int y, const uint32_t& original_color, const Point3D& normal) const;
	void GenerateHeightNormals(const uint32_t* pixels);
};
