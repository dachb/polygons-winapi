#include "stdafx.h"
#include "States.h"
#include "resource.h"
#include <winuser.h>
#include <algorithm>
#include <cassert>

VertexContextMenuState::VertexContextMenuState(Poly* polygon, VertexNode* vertex_node)
{
	assert(polygon != nullptr, "Polygon cannot be null");
	assert(vertex_node != nullptr, "Vertex cannot be null");
	this->polygon = polygon;
	this->vertex_node = vertex_node;
}

void VertexContextMenuState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	if (!vertex_node->current->CheckHit(x, y)) {
		isDone = true;
		nextState = new IdleState();
		return;
	}
	auto hMenu = CreatePopupMenu();
	POINT point;
	point.x = x;
	point.y = y;
	ClientToScreen(canvas_data.hWnd, &point);
	InsertMenu(hMenu, 0, MF_BYPOSITION | MF_STRING, DELETE_VERTEX, T_DELETE_VERTEX);
	TrackPopupMenu(hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, canvas_data.hWnd, nullptr);
}

void VertexContextMenuState::Command(const CanvasData& canvas_data, const int& command)
{
	auto& polygons = canvas_data.polygons;
	switch (command)
	{
	case DELETE_VERTEX:
		if (polygon->VertexCount() > 3)
			polygon->RemoveVertex(vertex_node);
		else
		{
			polygons.erase(remove(polygons.begin(), polygons.end(), polygon), polygons.end());
			delete polygon;
		}
		break;
	default: 
		break;
	}
	isDone = true;
	nextState = new IdleState();
}
