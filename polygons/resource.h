//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by polygons.rc
//
#define IDC_MYICON                      2
#define IDD_POLYGONS_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDS_STRING104                   104
#define IDS_DELETE                      104
#define IDI_POLYGONS                    107
#define IDI_SMALL                       108
#define IDC_POLYGONS                    109
#define IDR_MAINFRAME                   128
#define IDR_MAIN_MENU                   130
#define IDB_BITMAP1                     131
#define IDB_TILE                        131
#define IDB_BUMPMAP                     133
#define ID_OPCJE_WCZYTAJT               32771
#define ID_OPCJE_ZMIE32772              32772
#define ID_OPCJE_ANIMOWANE32773         32773
#define ID_OPCJE_                       32774
#define ID_OPCJE_WYBIERZFUNKCJ32775     32775
#define ID_WYBIERZFUNKCJ32776           32776
#define ID_WYBIERZFUNKCJ32777           32777
#define ID_WYBIERZFUNKCJ32778           32778
#define ID_WYBIERZFUNKCJ32779           32779
#define ID_WYBIERZFUNKCJ32780           32780
#define ID_WYBIERZFUNKCJ32781           32781
#define ID_WYBIERZFUNKCJ32782           32782
#define ID_FUNC_SCREEN                  32783
#define ID_FUNC_AFFINE_PLANE            32784
#define ID_FUNC_PARABOLOID              32785
#define ID_FUNC_LOGARITHM               32786
#define ID_FUNC_PYRAMID                 32787
#define ID_FUNC_CONE                    32788
#define ID_FUNC_SPHERE                  32789
#define ID_OPCJE_WCZYTAJHEIGHTMAP32790  32790
#define ID_WYBIERZFUNKCJ32791           32791
#define ID_FUNC_RIPPLE                  32792
#define ID_WYBIERZFUNKCJ32793           32793
#define ID_FUNC_BUMPS                   32794
#define ID_WYBIERZFUNKCJ32795           32795
#define ID_FUNC_CUT_PYRAMID             32796
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32797
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
