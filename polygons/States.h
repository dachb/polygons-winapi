﻿#pragma once
#include "stdafx.h"
#include "Primitives.h"
#include <vector>

struct CanvasData
{
	CanvasData(const HWND& wnd, std::vector<Poly*>& polygons, Renderer& renderer)
		: hWnd(wnd),
		polygons(polygons),
		renderer(renderer)
	{
	}

	const HWND& hWnd;
	std::vector<Poly*>& polygons;
	Renderer& renderer;
};


enum MouseButton
{
	LEFT_BUTTON = 1,
	RIGHT_BUTTON
};

enum VertexContextMenuItem
{
	DELETE_VERTEX = 101
};

enum SegmentContextMenuItem
{
	BISECT_SEGMENT = 201
};

enum PolygonContextMenuItem
{
	DELETE_POLYGON = 301,
	SCREEN_PLANE,
	PARABOLOID,
	AFFINE_PLANE,
	LOGARITHMIC_FUNCTION,
	PYRAMID,
	CONE,
	SPHERE
};

enum IntersectionContextMenuItem
{
	INTERSECT = 401
};

class State
{
public:
	virtual ~State() {}
	virtual void MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y) {}
	virtual void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) {}
	virtual void MouseMove(const CanvasData& canvas_data, const int& x, const int& y) {}
	virtual void KeyDown(const CanvasData& canvas_data, const WPARAM& keyCode) {}
	virtual void KeyUp(const CanvasData& canvas_data, const WPARAM& keyCode) {}
	virtual void Command(const CanvasData& canvas_data, const int& command) {}
	State* nextState = nullptr;
	bool isDone = false;
};

class StateManager
{
public:
	explicit StateManager(const HWND& hWnd);
	~StateManager();
	void ExecuteStateAction(const CanvasData& canvas_data, const UINT& message, const WPARAM& wParam, const LPARAM& lParam) const;
	void HandleEvent(const CanvasData& canvas_data, const UINT& message, const WPARAM& wParam, const LPARAM& lParam);
private:
	State* currentState;
	const HWND& hWnd;
};

class IdleState : public State
{
public:
	IdleState() {}
	void MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y) override;
	void Command(const CanvasData& canvas_data, const int& command) override;
	void KeyDown(const CanvasData& canvas_data, const WPARAM& keyCode) override;
private:
	void ChangeLightColor(const CanvasData& canvas_data);
	void ToggleLight(const CanvasData& canvas_data);
	HBITMAP OpenFile(const CanvasData& canvas_data);
	
	static const int MAX_FILE_NAME = 300;
	wchar_t file_name[MAX_FILE_NAME];
};

class LmbState : public State
{
public:
	LmbState() {}
	bool CheckVertices(std::vector<Poly*>& polygons, const int& x, const int& y);
	bool CheckPolygons(const WPARAM& wParam, const std::vector<Poly*>& polygons, const int x, const int y);
	void MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y) override;
private:
	Poly* first_selected = nullptr;
};

class IntersectionState : public State
{
public:
	IntersectionState(Poly* first, Poly* second);
	void MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y) override;
	void Command(const CanvasData& canvas_data, const int& command) override;
private:
	Poly *first, *second;

	const wchar_t* T_INTERSECT = L"Wyznacz przecięcie...";
};

class DrawingState : public State
{
public:
	DrawingState();
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
	void MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y) override;
	void MouseMove(const CanvasData& canvas_data, const int& x, const int& y) override;
	void KeyDown(const CanvasData& canvas_data, const WPARAM& keyCode) override;
private:
	Poly* current_polygon;
};

class MovingVertexState : public State
{
public:
	explicit MovingVertexState(Vertex& vertex) : vertex(vertex) {}
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
	void MouseMove(const CanvasData& canvas_data, const int& x, const int& y) override;
private:
	Vertex& vertex;
};

class MovingPolygonState : public State
{
public:
	explicit MovingPolygonState(Poly* polygon);
	void MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y) override;
	void MouseMove(const CanvasData& canvas_data, const int& x, const int& y) override;
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
private:
	Poly* polygon;
	int old_x, old_y;
};

class RmbState : public State
{
public:
	RmbState() {}
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
};

class VertexContextMenuState : public State
{
public:
	VertexContextMenuState(Poly* polygon, VertexNode* vertex_node);
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
	void Command(const CanvasData& canvas_data, const int& command) override;
private:
	Poly* polygon;
	VertexNode* vertex_node;

	const wchar_t* T_DELETE_VERTEX = L"Usuń...";
};

class SegmentContextMenuState : public State
{
public:
	SegmentContextMenuState(Poly* polygon, SegmentNode* segment_node);
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
	void Command(const CanvasData& canvas_data, const int& command) override;
private:
	Poly* polygon;
	SegmentNode* segment_node;

	const wchar_t* T_BISECT_SEGMENT = L"Podziel...";
};

class PolygonContextMenuState : public State
{
public:
	explicit PolygonContextMenuState(Poly* polygon);
	void MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y) override;
	void Command(const CanvasData& canvas_data, const int& command) override;
private:
	Poly* polygon;

	const wchar_t* T_DELETE_POLYGON = L"Usuń...";
};