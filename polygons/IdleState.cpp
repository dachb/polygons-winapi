#include "stdafx.h"
#include "States.h"
#include "Renderer.h"
#include "resource.h"
#include <commdlg.h>

void IdleState::MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y)
{
	switch (button)
	{
	case LEFT_BUTTON:
		isDone = true;
		nextState = new LmbState();
		break;
	case RIGHT_BUTTON:
		isDone = true;
		nextState = new RmbState();
		break;
	default:
		break;
	}
}

void IdleState::Command(const CanvasData& canvas_data, const int& command)
{
	HBITMAP hBitmap;
	switch (command) {
	case ID_OPCJE_WCZYTAJT:
		hBitmap = OpenFile(canvas_data);
		if (hBitmap != nullptr)
		{
			canvas_data.renderer.SetBackground(hBitmap);
			DeleteObject(hBitmap);
		}
		break;
	case ID_OPCJE_WCZYTAJHEIGHTMAP32790:
		hBitmap = OpenFile(canvas_data);
		if (hBitmap != nullptr)
		{
			canvas_data.renderer.LoadBumpMap(hBitmap);
			DeleteObject(hBitmap);
		}
		break;
	case ID_OPCJE_ZMIE32772:
		ChangeLightColor(canvas_data);
		break;
	case ID_OPCJE_ANIMOWANE32773:
		ToggleLight(canvas_data);
		break;
	case ID_FUNC_SCREEN:
		canvas_data.renderer.SetFunction(ScreenPlane);
		break;
	case ID_FUNC_AFFINE_PLANE:
		canvas_data.renderer.SetFunction(AffinePlane);
		break;
	case ID_FUNC_PARABOLOID:
		canvas_data.renderer.SetFunction(Paraboloid);
		break;
	case ID_FUNC_LOGARITHM:
		canvas_data.renderer.SetFunction(LogarithmicFunction);
		break;
	case ID_FUNC_CUT_PYRAMID:
		canvas_data.renderer.SetFunction(CutPyramid);
		break;
	case ID_FUNC_PYRAMID:
		canvas_data.renderer.SetFunction(Pyramid);
		break;
	case ID_FUNC_CONE:
		canvas_data.renderer.SetFunction(Cone);
		break;
	case ID_FUNC_SPHERE:
		canvas_data.renderer.SetFunction(Sphere);
		break;
	case ID_FUNC_RIPPLE:
		canvas_data.renderer.SetFunction(Ripple);
		break;
	case ID_FUNC_BUMPS:
		canvas_data.renderer.SetFunction(Bumps);
		break;
	default:
		break;
	}
}

void IdleState::KeyDown(const CanvasData& canvas_data, const WPARAM& keyCode)
{
	switch (keyCode)
	{
	case VK_UP:
		canvas_data.renderer.MoveLight(UP);
		break;
	case VK_DOWN:
		canvas_data.renderer.MoveLight(DOWN);
		break;
	case VK_LEFT:
		canvas_data.renderer.MoveLight(LEFT);
		break;
	case VK_RIGHT:
		canvas_data.renderer.MoveLight(RIGHT);
		break;
	case VK_ADD:
		canvas_data.renderer.MoveLight(AWAY_FROM_SCREEN);
		break;
	case VK_SUBTRACT:
		canvas_data.renderer.MoveLight(TOWARD_SCREEN);
		break;
	default: break;
	}
}

void IdleState::ChangeLightColor(const CanvasData& canvas_data)
{
	CHOOSECOLOR ccol{};
	COLORREF colors[16] = {};
	ccol.lStructSize = sizeof(CHOOSECOLOR);
	ccol.lpCustColors = colors;
	ccol.hwndOwner = canvas_data.hWnd;
	ccol.Flags = CC_ANYCOLOR | CC_FULLOPEN;
	if (ChooseColor(&ccol) == TRUE)
	{
		canvas_data.renderer.SetLightColor(ccol.rgbResult);
	}
}

void IdleState::ToggleLight(const CanvasData& canvas_data)
{
	auto light_status = canvas_data.renderer.ToggleUniformLight();
	auto hMenu = GetMenu(canvas_data.hWnd);
	auto flag = light_status ? MF_UNCHECKED : MF_CHECKED;
	CheckMenuItem(hMenu, ID_OPCJE_ANIMOWANE32773, flag | MF_BYCOMMAND);
}

HBITMAP IdleState::OpenFile(const CanvasData& canvas_data)
{
	OPENFILENAME ofn{};
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = canvas_data.hWnd;
	ofn.lpstrFile = file_name;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(file_name);
	ofn.lpstrFilter = L"Wszystkie pliki (*.*)\0*.*\0Bitmapy (.bmp)\0*.bmp\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = nullptr;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = nullptr;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn) == TRUE)
	{
		return (HBITMAP)LoadImage(nullptr, ofn.lpstrFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	}
	return nullptr;
}
