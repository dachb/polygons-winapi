#include "stdafx.h"
#include "States.h"

void MovingVertexState::MouseMove(const CanvasData& canvas_data, const int& x, const int& y)
{
	vertex.Move(x, y);
}

void MovingVertexState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	isDone = true;
	nextState = new IdleState();
}
