#include "stdafx.h"
#include "Primitives.h"

bool Segment::CheckHit(const int& x, const int& y) const
{
	auto A = end.y - start.y;
	auto B = end.x - start.x;
	auto C = end * start;
	double d = abs(A * x - B * y + C);
	return d * d <= MAX_DISTANCE * MAX_DISTANCE * (A * A + B * B) &&
		InRectangle(x, y, *this, MAX_DISTANCE / 2);
}

int Segment::Intersects(const Segment& other) const
{
	long long d1 = (other.end - other.start) * (this->start - other.start);
	long long d2 = (other.end - other.start) * (this->end - other.start);
	long long d3 = (this->end - this->start) * (other.start - this->start);
	long long d4 = (this->end - this->start) * (other.end - this->start);

	auto d12 = sgn(d1) * sgn(d2);
	auto d34 = sgn(d3) * sgn(d4);

	if (d12 > 0 || d34 > 0) return 0;
	if (d12 < 0 || d34 < 0) return 1;

	return InRectangle(this->start, other) ||
		InRectangle(this->end, other) ||
		InRectangle(other.start, *this) ||
		InRectangle(other.end, *this) ? 2 : 0;
}

Vertex Segment::IntersectionPoint(const Segment& other) const
{
	auto v1(this->start), v2(this->end), v3(other.start), v4(other.end);
	if (v1.x == v2.x || v3.x == v4.x)
	{
		if (v3.x == v4.x)
		{
			std::swap(v1.x, v3.x);
			std::swap(v2.x, v4.x);
			std::swap(v1.y, v3.y);
			std::swap(v2.y, v4.y);
		}
		auto a = static_cast<double>(v4.y - v3.y) / (v4.x - v3.x);
		auto b = v3.y - a * v3.x;
		return Vertex(v1.x, a * v1.x + b);
	}
	auto a1 = static_cast<double>(v2.y - v1.y) / (v2.x - v1.x);
	auto b1 = v1.y - a1 * v1.x;
	auto a2 = static_cast<double>(v4.y - v3.y) / (v4.x - v3.x);
	auto b2 = v3.y - a2 * v3.x;
	auto x = (b2 - b1) / (a1 - a2);
	return Vertex(x, a1 * x + b1);
}


int Segment::InRectangle(const Vertex& v, const Segment& s)
{
	return InRectangle(v.x, v.y, s, 0) ? 2 : 0;
}

bool Segment::InRectangle(const int& x, const int& y, const Segment& s, const int& tolerance)
{
	return min(s.start.x, s.end.x) - tolerance <= x &&
		x <= max(s.start.x, s.end.x) + tolerance &&
		min(s.start.y, s.end.y) - tolerance <= y &&
		y <= max(s.start.y, s.end.y) + tolerance;
}
