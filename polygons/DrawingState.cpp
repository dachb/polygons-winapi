#include "stdafx.h"
#include "States.h"
#include <algorithm>

DrawingState::DrawingState()
{
	current_polygon = new Poly();
}

void DrawingState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	if (current_polygon->CanClose(x, y))
	{
		current_polygon->Close();
		isDone = true;
		nextState = new IdleState();
		return;
	}
	current_polygon->AddVertex(x, y);
}

void DrawingState::MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y)
{
	if (current_polygon->VertexCount() == 0)
	{
		canvas_data.polygons.push_back(current_polygon);
		current_polygon->AddVertex(x, y);
	}
}

void DrawingState::MouseMove(const CanvasData& canvas_data, const int& x, const int& y)
{
	current_polygon->MoveLastVertex(x, y);
}

void DrawingState::KeyDown(const CanvasData& canvas_data, const WPARAM& keyCode)
{
	if (keyCode == VK_ESCAPE)
	{		
		canvas_data.polygons.erase(remove(canvas_data.polygons.begin(), canvas_data.polygons.end(), current_polygon), canvas_data.polygons.end());
		delete current_polygon;
		isDone = true;
		nextState = new IdleState();
	}
}
