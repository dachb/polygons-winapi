#include "stdafx.h"
#include "States.h"

void RmbState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	isDone = true;
	if (button != RIGHT_BUTTON)
	{
		nextState = new IdleState();
		return;
	}
	VertexNode* vertex_hit;
	SegmentNode* segment_hit;
	for (auto it = canvas_data.polygons.rbegin(); it != canvas_data.polygons.rend(); ++it)
	{
		vertex_hit = (*it)->FindVertex(x, y);
		if (vertex_hit != nullptr)
		{
			nextState = new VertexContextMenuState(*it, vertex_hit);
			return;
		}
		segment_hit = (*it)->FindSegment(x, y);
		if (segment_hit != nullptr)
		{
			nextState = new SegmentContextMenuState(*it, segment_hit);
			return;
		}
		if ((*it)->CheckHit(x, y))
		{
			nextState = new PolygonContextMenuState(*it);
			return;
		}
	}
	nextState = new IdleState();
}