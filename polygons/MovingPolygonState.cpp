#include "stdafx.h"
#include "States.h"

MovingPolygonState::MovingPolygonState(Poly* polygon)
{
	this->polygon = polygon;
	old_x = old_y = 0;
}

void MovingPolygonState::MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y)
{
	this->old_x = x;
	this->old_y = y;
}

void MovingPolygonState::MouseMove(const CanvasData& canvas_data, const int& x, const int& y)
{
	polygon->TranslateVertices(x - old_x, y - old_y);
	this->old_x = x;
	this->old_y = y;
}

void MovingPolygonState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	isDone = true;
	nextState = new IdleState();
}
