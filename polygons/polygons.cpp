// polygons.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "polygons.h"
#include "States.h"
#include <vector>
#include "Primitives.h"
#include "DataStructures.h"
#include "Renderer.h"
#include <winuser.h>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szMenuName[MAX_LOADSTRING];
Renderer renderer(640, 480);
std::vector<Poly*> polygons;
StateManager* state_manager;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_POLYGONS, szWindowClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDR_MAIN_MENU, szMenuName, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_POLYGONS));

    MSG msg;
	

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_POLYGONS));
    wcex.hCursor        = LoadCursor(nullptr, IDC_CROSS);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= nullptr;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, LoadMenu(hInstance, MAKEINTRESOURCE(IDR_MAIN_MENU)), hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
   HBITMAP hTile = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_TILE));
   renderer.SetBackground(hTile);
   HBITMAP hBump = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_BUMPMAP));
   renderer.LoadBumpMap(hBump);

   state_manager = new StateManager(hWnd);
   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

void AddExamplePolygons()
{
	auto first = new Poly();
	first->AddVertex(10, 10);
	first->AddVertex(300, 50);
	first->AddVertex(600, 400);
	first->AddVertex(200, 600);
	first->AddVertex(10, 10);
	first->Close();
	polygons.push_back(first);

	auto second = new Poly();
	second->AddVertex(700, 100);
	second->AddVertex(100, 150);
	second->AddVertex(300, 350);
	second->AddVertex(500, 200);
	second->AddVertex(750, 500);
	second->AddVertex(700, 100);
	second->Close();
	polygons.push_back(second);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int width, height;
	CanvasData canvas_data(hWnd, polygons, renderer);
    switch (message)
	{
	case WM_CREATE:
		SetTimer(hWnd, 7, 30, nullptr);
		AddExamplePolygons();
		break;
	case WM_TIMER:
		renderer.RotateLight();
		InvalidateRect(hWnd, nullptr, FALSE);
		break;
	case WM_ERASEBKGND:
		return 1;
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
	case WM_KEYDOWN:
	case WM_KEYUP:
	case WM_COMMAND:
		state_manager->HandleEvent(canvas_data, message, wParam, lParam);
		InvalidateRect(hWnd, nullptr, FALSE);
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		renderer.Resized(width, height);
		InvalidateRect(hWnd, nullptr, FALSE);
		break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
			renderer.Draw(polygons);
			BitBlt(hdc, 0, 0, renderer.width, renderer.height, renderer.hBitmapDC, 0, 0, SRCCOPY);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		delete state_manager;
		for (auto it = polygons.begin(); it != polygons.end(); ++it)
		{
			delete *it;
		}
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}