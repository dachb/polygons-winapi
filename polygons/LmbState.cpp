#include "stdafx.h"
#include "States.h"

bool LmbState::CheckVertices(std::vector<Poly*>& polygons, const int& x, const int& y)
{
	VertexNode* vertex_hit;
	for (auto it = polygons.rbegin(); it != polygons.rend(); ++it)
	{
		vertex_hit = (*it)->FindVertex(x, y);
		if (vertex_hit != nullptr)
		{
			nextState = new MovingVertexState(*vertex_hit->current);
			isDone = true;
			return true;
		}
	}
	return false;
}

bool LmbState::CheckPolygons(const WPARAM& wParam, const std::vector<Poly*>& polygons, const int x, const int y)
{
	for (auto it = polygons.rbegin(); it != polygons.rend(); ++it)
	{
		if ((*it)->CheckHit(x, y))
		{
			if (wParam & MK_CONTROL && *it != first_selected)
			{
				(*it)->SetSelection(true);
				if (first_selected == nullptr) first_selected = *it;
				else
				{
					nextState = new IntersectionState(first_selected, *it);
					isDone = true;
				}
				return true;
			}
			if (first_selected != nullptr) first_selected->SetSelection(false);
			first_selected = nullptr;
			nextState = new MovingPolygonState(*it);
			isDone = true;
			return true;
		}
	}
	return false;
}

void LmbState::MouseDown(const CanvasData& canvas_data, WPARAM wParam, MouseButton button, const int& x, const int& y)
{
	if (CheckVertices(canvas_data.polygons, x, y)) return;
	if (CheckPolygons(wParam, canvas_data.polygons, x, y)) return;
	nextState = new DrawingState();
	if (first_selected != nullptr) first_selected->SetSelection(false);
	first_selected = nullptr;
	isDone = true;
}
