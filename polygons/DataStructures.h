#pragma once
#include <list>
class Segment;
class Vertex;

struct SegmentNode
{
	SegmentNode(SegmentNode* previous, const Segment& segment, SegmentNode* next);
	explicit SegmentNode(const Segment& segment) : SegmentNode(nullptr, segment, nullptr) {}
	SegmentNode(const SegmentNode& segment) : SegmentNode(segment.previous, *segment.current, segment.next) {}
	SegmentNode& operator=(SegmentNode other);
	~SegmentNode();

	void Reverse();

	SegmentNode* previous;
	Segment* current;
	SegmentNode* next;
};

struct VertexNode
{
	VertexNode(VertexNode* previous, const Vertex& vertex, VertexNode* next, SegmentNode* in_segment, SegmentNode* out_segment);
	explicit VertexNode(const Vertex& vertex) : VertexNode(nullptr, vertex, nullptr, nullptr, nullptr) {}
	VertexNode(const VertexNode& vertex) : VertexNode(vertex.previous, *vertex.current, vertex.next, vertex.in_segment, vertex.out_segment) {}
	VertexNode& operator=(VertexNode other);
	~VertexNode();

	void Reverse();

	VertexNode* previous;
	Vertex* current;
	VertexNode* next;
	SegmentNode* in_segment;
	SegmentNode* out_segment;
};

class SegmentVertexList
{
public:
	SegmentVertexList() {}
	SegmentVertexList(const SegmentVertexList& list);
	SegmentVertexList& operator=(const SegmentVertexList& that) = delete;
	~SegmentVertexList();

	void AddVertex(const Vertex& vertex);
	void ClosePolygon();
	void RemoveVertex(VertexNode* vertex_node);
	VertexNode* BisectSegment(SegmentNode* segment_node, Vertex midpoint);
	void BisectSegment(SegmentNode* segment_node);
	void Reverse();
	VertexNode* FindVertex(const int& x, const int& y);
	SegmentNode* FindSegment(const int& x, const int& y);

	VertexNode* FirstVertex() const { return vertex_head; }
	VertexNode* LastVertex() const { return vertex_tail; }
	SegmentNode* FirstSegment() const { return segment_head; }
	SegmentNode* LastSegment() const { return segment_tail; }
	std::list<VertexNode*> VerticesToList() const;
	int segment_count = 0;
	int vertex_count = 0;
private:
	SegmentNode* segment_head = nullptr;
	SegmentNode* segment_tail = nullptr;
	VertexNode* vertex_head = nullptr;
	VertexNode* vertex_tail = nullptr;
};

struct ActiveEdge
{
	const Vertex* start;
	const Vertex* end;
	int y_max;
	float x;
	float slope;
};