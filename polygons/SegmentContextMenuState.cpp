#include "stdafx.h"
#include "States.h"
#include <cassert>

SegmentContextMenuState::SegmentContextMenuState(Poly* polygon, SegmentNode* segment_node)
{
	assert(polygon != nullptr, "Polygon cannot be null");
	assert(segment_node != nullptr, "Vertex cannot be null");
	this->polygon = polygon;
	this->segment_node = segment_node;
}

void SegmentContextMenuState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	if (!segment_node->current->CheckHit(x, y)) {
		isDone = true;
		nextState = new IdleState();
		return;
	}
	auto hMenu = CreatePopupMenu();
	POINT point;
	point.x = x;
	point.y = y;
	ClientToScreen(canvas_data.hWnd, &point);
	InsertMenu(hMenu, 0, MF_BYPOSITION | MF_STRING, BISECT_SEGMENT, T_BISECT_SEGMENT);
	TrackPopupMenu(hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, canvas_data.hWnd, nullptr);
}

void SegmentContextMenuState::Command(const CanvasData& canvas_data, const int& command)
{
	switch (command)
	{
	case BISECT_SEGMENT:
		polygon->Bisect(segment_node);
		break;
	default:
		break;
	}
	isDone = true;
	nextState = new IdleState();
}
