#pragma once
#include "DataStructures.h"
#include <vector>

struct Point3D
{
	Point3D() : x(0), y(0), z(0) {}

	Point3D(double x, double y, double z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	double x, y, z;
	double length() const;
	Point3D normalize() const;
	Point3D& operator+=(const Point3D& rhs);
	Point3D& operator-=(const Point3D& rhs);
	Point3D& operator*=(const double& rhs);
	Point3D& operator/=(const double& rhs);

	friend Point3D operator+(Point3D lhs, Point3D rhs)
	{
		return lhs += rhs;
	}

	friend Point3D operator-(Point3D lhs, Point3D rhs)
	{
		return lhs -= rhs;
	}

	friend double operator*(Point3D lhs, Point3D rhs)
	{
		return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
	}

	friend Point3D operator*(Point3D lhs, double rhs)
	{
		return lhs *= rhs;
	}

	friend Point3D operator/(Point3D lhs, double rhs)
	{
		return lhs /= rhs;
	}
};

class Vertex
{
public:
	int x, y;

	Vertex(const int& x, const int& y);

	bool CheckHit(const int& x, const int& y) const;
	void Move(const int& x, const int& y);
	void Translate(const int& dx, const int& dy);

	Vertex& operator+=(const Vertex& rhs);
	Vertex& operator-=(const Vertex& rhs);
	Vertex& operator/=(const int& rhs);
	Vertex& operator=(Vertex rhs);
	long operator*(const Vertex& rhs) const;

	friend Vertex operator+(Vertex lhs, const Vertex& rhs);
	friend Vertex operator-(Vertex lhs, const Vertex& rhs);
	friend Vertex operator/(Vertex lhs, const int& rhs);
private:
	const int MARKER_SIZE = 7;

	friend class Renderer;
};

class Segment
{
public:
	Vertex& start;
	Vertex& end;

	Segment(Vertex& start, Vertex& end) : start(start), end(end) {}

	bool CheckHit(const int& x, const int& y) const;
	int Intersects(const Segment& other) const;
	Vertex IntersectionPoint(const Segment& other) const;
private:
	const int MAX_DISTANCE = 4;
	static int InRectangle(const Vertex& v, const Segment& s);
	static bool InRectangle(const int& x, const int& y, const Segment& s, const int& tolerance);

	friend class Renderer;
};

struct Intersection
{
	Intersection(VertexNode* first, VertexNode* second, bool first_to_second)
	{
		this->first = first;
		this->second = second;
		this->first_to_second = first_to_second;
		this->visited = false;
	}

	VertexNode* first;
	VertexNode* second;
	bool first_to_second;
	bool visited;
};

class Poly
{
public:
	void AddVertex(const int& x, const int& y);
	void MoveLastVertex(const int& x, const int& y);
	bool CanClose(const int& x, const int& y) const;
	int VertexCount() const { return list.vertex_count; }
	void Close();
	VertexNode* FindVertex(const int& x, const int& y);
	SegmentNode* FindSegment(const int& x, const int& y);
	void RemoveVertex(VertexNode* vertex_node);
	void Bisect(SegmentNode* segment_node);
	bool CheckHit(const int& x, const int& y) const;
	void TranslateVertices(const int& dx, const int& dy);
	void SetSelection(bool selected);
	std::vector<Poly*> Intersect(Poly& other);
private:
	void RemoveCollinearPoints(const Poly& poly);
	bool SelfIntersecting() const;
	long SignedArea() const;
	SegmentVertexList list;
	bool closed = false;
	bool selected = false;

	friend class Renderer;
};

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}