﻿#include "stdafx.h"
#include "Renderer.h"
#include <list>
#include <algorithm>

void Renderer::CreateBitmap(const int& width, const int& height, HDC& hdc, HBITMAP& hBitmap, HBITMAP& hOldBitmap, uint32_t*& pixels)
{
	hdc = CreateCompatibleDC(nullptr);
	BITMAPINFO bitmapinfo{};
	bitmapinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapinfo.bmiHeader.biWidth = width;
	bitmapinfo.bmiHeader.biHeight = -height;
	bitmapinfo.bmiHeader.biPlanes = 1;
	bitmapinfo.bmiHeader.biBitCount = 32;
	bitmapinfo.bmiHeader.biCompression = BI_RGB;
	hBitmap = CreateDIBSection(hdc, &bitmapinfo, DIB_RGB_COLORS, (void**)&pixels, nullptr, 0);
	hOldBitmap = (HBITMAP)SelectObject(hdc, hBitmap);
}

Renderer::Renderer(const int& width, const int& height) : light_source(width / 2, height / 2, 100)
{
	CreateBitmap(width, height, hBitmapDC, hBitmap, hOldBitmap, pixels);
	RotateLight();
	CalculateSurfaceNormals();
	light_center = Point3D(0, 0, 500);
	light_radius = min(width, height) / 2;
}

void Renderer::CleanupBitmap(HDC& hdc, HBITMAP& hBitmap, HBITMAP& hOldBitmap)
{
	SelectObject(hdc, hOldBitmap);
	DeleteDC(hdc);
	DeleteObject(hBitmap);
}

Renderer::~Renderer()
{
	CleanupBitmap(hBitmapDC, hBitmap, hOldBitmap);
	CleanupBitmap(hTileDC, hTile, hOldTile);
}

void Renderer::Resized(const int& width, const int& height)
{
	this->width = width;
	this->height = height;
	CleanupBitmap(hBitmapDC, hBitmap, hOldBitmap);
	CreateBitmap(width, height, hBitmapDC, hBitmap, hOldBitmap, pixels);
	RotateLight();
	CalculateSurfaceNormals();
}

void Renderer::Draw(std::vector<Poly*> polygons)
{
	DrawBackground();
	for (auto it = polygons.begin(); it != polygons.end(); ++it)
	{
		Draw(**it);
	}
}

void Renderer::SetBackground(const HBITMAP& hBitmap)
{
	BITMAP bitmap;
	auto hTempDC = CreateCompatibleDC(nullptr);
	auto hOldBitmap = (HBITMAP)SelectObject(hTempDC, hBitmap);
	GetObject(hBitmap, sizeof(BITMAP), &bitmap);
	tile_width = bitmap.bmWidth;
	tile_height = bitmap.bmHeight;
	CleanupBitmap(hTileDC, hTile, hOldTile);
	CreateBitmap(tile_width, tile_height, hTileDC, hTile, hOldTile, tile_pixels);
	BitBlt(hTileDC, 0, 0, tile_width, tile_height, hTempDC, 0, 0, SRCCOPY);
	SelectObject(hTempDC, hOldBitmap);
	DeleteDC(hTempDC);
}

void Renderer::LoadBumpMap(const HBITMAP& hBitmap)
{
	BITMAP bitmap;
	HBITMAP hCopy, hOldCopy;
	auto hCopyDC = CreateCompatibleDC(nullptr);
	auto hBitmapDC = CreateCompatibleDC(nullptr);
	auto hOldBitmap = (HBITMAP)SelectObject(hBitmapDC, hBitmap);
	GetObject(hBitmap, sizeof(BITMAP), &bitmap);
	bump_width = bitmap.bmWidth;
	bump_height = bitmap.bmHeight;
	auto temp_pixels = new uint32_t[bump_width * bump_height];
	CreateBitmap(bump_width, bump_height, hCopyDC, hCopy, hOldCopy, temp_pixels);
	BitBlt(hCopyDC, 0, 0, bump_width, bump_height, hBitmapDC, 0, 0, SRCCOPY);
	GenerateHeightNormals(temp_pixels);
	CleanupBitmap(hCopyDC, hCopy, hOldCopy);
	SelectObject(hBitmapDC, hOldBitmap);
	DeleteDC(hBitmapDC);
}

void Renderer::SetLightColor(const COLORREF& color)
{
	light_color = RGB2BGR(color);
}

void Renderer::SetFunction(Point3D(* f)(int, int, int, int))
{
	surface_normal_function = f;
	CalculateSurfaceNormals();
}

void Renderer::CalculateSurfaceNormals()
{
	if (surface_normals != nullptr)
		delete[] surface_normals;
	surface_normals = new Point3D[width * height];
	auto it = surface_normals;
	for (auto y = 0; y < height; ++y)
		for (auto x = 0; x < width; ++x, ++it)
		{
			*it = surface_normal_function(x - width / 2.0, y - height / 2.0, width, height);
			*it /= it->z;
		}
}

void Renderer::RotateLight()
{
	angle = (angle + 1) % 3600;
	auto x = light_radius * cos(angle / 10.0);
	auto y = light_radius * sin(angle / 10.0);
	light_source = Point3D(x, y, 0) + light_center;
}

bool Renderer::ToggleUniformLight()
{
	is_uniform = !is_uniform;
	return is_uniform;
}

void Renderer::MoveLight(LightMovementDirection direction)
{
	switch (direction)
	{
	case LEFT: 
		light_center.x += increment;
		break;
	case RIGHT: 
		light_center.x -= increment;
		break;
	case UP: 
		light_center.y += increment;
		break;
	case DOWN: 
		light_center.y -= increment;
		break;
	case TOWARD_SCREEN: 
		light_center.z -= 2 * increment;
		break;
	case AWAY_FROM_SCREEN: 
		light_center.z += 2 * increment;
		break;
	default: break;
	}
}

void Renderer::DrawBackground()
{
	memset(pixels, 0xff, 4 * width * height);
}

void Renderer::Draw(const Poly& polygon)
{
	if (polygon.closed) Fill(polygon);
	uint32_t color = polygon.selected ? COLOR(0, 0, 255) : COLOR(0, 0, 0);
	Draw(polygon.list, color);
}

void Renderer::Fill(const Poly& polygon)
{
	std::vector<ActiveEdge> active_edge_table;
	auto vertices = polygon.list.VerticesToList();
	vertices.sort([](const VertexNode* v1, const VertexNode* v2) -> bool { return v1->current->y < v2->current->y; });
	auto y_min = (*vertices.begin())->current->y;
	auto y_max = (*vertices.rbegin())->current->y;
	auto vertex_it = vertices.begin();
	if (y_min > height) return;
	for (auto y = y_min; y < min(y_max, height); ++y)
	{
		while ((*vertex_it)->current->y == y - 1)
		{
			ProcessNeighbors(*vertex_it, active_edge_table);
			++vertex_it;
		}
		std::sort(
			active_edge_table.begin(),
			active_edge_table.end(),
			[](const ActiveEdge& e1, const ActiveEdge& e2) -> bool { return e1.x < e2.x; }
		);
		for (size_t i = 0; i < active_edge_table.size() && y > 0; i += 2)
		{
			auto x0 = max(0, static_cast<int>(active_edge_table[i].x));
			auto x1 = min(static_cast<int>(active_edge_table[i + 1].x), width - 1);
			for (auto x = x0; x <= x1; ++x)
			{
				auto texture_color = tile_pixels[y % tile_height * tile_width + x % tile_width];
				auto normal = surface_normals[y * width + x];
				if (!is_uniform)
				{
					auto bump = height_normals[y % bump_height * bump_width + x % bump_width];
					auto t = Point3D(1, 0, -normal.x).normalize();
					auto b = Point3D(1, 0, -normal.y).normalize();
					normal = normal + t * bump.x + b * bump.y;
				}
				pixels[y * width + x] = LambertianReflection(x, y, texture_color, normal);
			}
		}
		for (auto it = active_edge_table.begin(); it != active_edge_table.end(); ++it)
			(*it).x += (*it).slope;
	}
}

void Renderer::ProcessNeighbors(VertexNode* vertex_node, std::vector<ActiveEdge>& active_edge_table)
{
	CheckEdge(vertex_node->previous->current, vertex_node->current, active_edge_table);
	CheckEdge(vertex_node->next->current, vertex_node->current, active_edge_table);
}

void Renderer::CheckEdge(const Vertex* from, const Vertex* to, std::vector<ActiveEdge>& active_edge_table)
{
	if (from->y > to->y)
	{
		ActiveEdge e;
		e.start = from;
		e.end = to;
		e.x = to->x;
		e.y_max = max(from->y, to->y);
		e.slope = static_cast<float>(from->x - to->x) / (from->y - to->y);
		active_edge_table.push_back(e);
	}
	if (from->y < to->y)
	{
		for (auto it = active_edge_table.begin(); it != active_edge_table.end(); ++it)
		{
			if (it->start->y != from->y && it->start->y != to->y) continue;
			active_edge_table.erase(it);
			return;
		}
	}
}

void Renderer::SetPixel(const int& x, const int& y, const int& color)
{
	if (x < 0 || y < 0 || x >= width || y >= height) return;
	pixels[width * y + x] = color;
}


void Renderer::Draw(const Vertex& vertex, const uint32_t& color)
{
	auto min_x = vertex.x - vertex.MARKER_SIZE / 2;
	auto max_x = vertex.x + vertex.MARKER_SIZE / 2;
	auto min_y = vertex.y - vertex.MARKER_SIZE / 2;
	auto max_y = vertex.y + vertex.MARKER_SIZE / 2;
	for (auto x = min_x; x <= max_x; x++)
		for (auto y = min_y; y <= max_y; y++)
			SetPixel(x, y, color);
}

void Renderer::Draw(const SegmentVertexList& list, const uint32_t& color)
{
	auto vertex_it = list.FirstVertex();
	for (auto i = 0; i < list.vertex_count; ++i, vertex_it = vertex_it->next)
		Draw(*vertex_it->current, color);

	auto segment_it = list.FirstSegment();
	for (auto i = 0; i < list.segment_count; ++i, segment_it = segment_it->next)
		Draw(*segment_it->current, color);
}

void Renderer::Draw(const Segment& segment, const uint32_t& color)
{
	Draw(segment.start, color);
	Draw(segment.end, color);

	auto x0 = segment.start.x;
	auto y0 = segment.start.y;
	auto x1 = segment.end.x;
	auto y1 = segment.end.y;

	auto steep = abs(y0 - y1) >= abs(x0 - x1);
	if (steep)
	{
		std::swap(x0, y0);
		std::swap(x1, y1);
	}
	if (x0 > x1)
	{
		std::swap(x0, x1);
		std::swap(y0, y1);
	}
	BresenhamLine(x0, y0, x1, y1, steep, color);
}

void Renderer::BresenhamLine(int x0, int y0, int x1, int y1, bool steep, const uint32_t& color)
{
	auto dx = x1 - x0;
	auto dy = abs(y1 - y0);
	auto d = 2 * dy - dx;
	auto increment = y0 > y1 ? -1 : 1;
	while (x0 <= x1)
	{
		if (d <= 0)
		{
			d += 2 * dy;
			x0++;
		}
		else
		{
			d += 2 * (dy - dx);
			x0++;
			y0 += increment;
		}
		steep ? SetPixel(y0, x0, color) : SetPixel(x0, y0, color);
	}
}

uint32_t Renderer::LambertianReflection(int x, int y, const uint32_t& original_color, const Point3D& normal) const
{
	x -= width / 2;
	y -= height / 2;
	auto red = RED(original_color) * RED(light_color) >> 8;
	auto green = GREEN(original_color) * GREEN(light_color) >> 8;
	auto blue = BLUE(original_color) * BLUE(light_color) >> 8;
	if (!is_uniform) {
		auto toLightSource = (light_source - Point3D(x, y, 0)).normalize();
		auto cosine = toLightSource * normal / normal.length();
		if (cosine < 0)
		{
			cosine = 0;
		}
		red *= cosine;
		green *= cosine;
		blue *= cosine;
	}
	return COLOR(red, green, blue);
}

void Renderer::GenerateHeightNormals(const uint32_t* pixels)
{
	if (height_normals != nullptr)
		delete[] height_normals;
	height_normals = new Point3D[bump_width * bump_height];
	auto normal_iter = height_normals;
	double left, right, top, bottom;
	for (auto y = 0; y < bump_height; ++y)
		for (auto x = 0; x < bump_width; ++x, ++normal_iter)
		{
			left = BLUE(pixels[y * bump_width + max(x - 1, 0)]) / 255.0;
			right = BLUE(pixels[y * bump_width + min(x + 1, bump_width - 1)]) / 255.0;
			top = BLUE(pixels[max(y - 1, 0) * bump_width + x]) / 255.0;
			bottom = BLUE(pixels[min(y + 1, bump_height - 1) * bump_width + x]) / 255.0;

			normal_iter->x = (left - right) / 2;
			normal_iter->y = (top - bottom) / 2;
			normal_iter->z = 1;
		}
}

Point3D ScreenPlane(int x, int y, int w, int h)
{
	return Point3D(0, 0, 1);
}

Point3D Paraboloid(int x, int y, int w, int h)
{
	return Point3D(x, y, 1);
}

Point3D AffinePlane(int x, int y, int w, int h)
{
	return Point3D(0.5, 0, 1);
}

Point3D LogarithmicFunction(int x, int y, int w, int h)
{
	if (x == 0 && y == 0) return Point3D(0, 0, 1);
	return Point3D(50.0 / x, 50.0 / y, 1);
}

Point3D Pyramid(int x, int y, int w, int h)
{
	return Point3D(x < 0 ? -1 : 1, y < 0 ? -1 : 1, 1);
}

Point3D CutPyramid(int x, int y, int w, int h)
{
	RECT cut_off;
	cut_off.left = -w / 4;
	cut_off.right = w / 4;
	cut_off.top = -h / 4;
	cut_off.bottom = h / 4;
	if (x > cut_off.left && x < cut_off.right &&
		y > cut_off.top && y < cut_off.bottom)
	{
		return Point3D(0, 0, 1);
	}
	auto x_scaling_factor = (double)w / h;
	x /= x_scaling_factor;
	return Point3D(sgn(x - y) + sgn(x + y), -sgn(x - y) + sgn(x + y), 1) * 2;
}

Point3D Cone(int x, int y, int w, int h)
{
	return Point3D(-x / sqrt(x*x + y*y), -y / sqrt(x*x + y*y), 1);
}

Point3D Sphere(int x, int y, int w, int h)
{
	return Point3D(x / max(1, sqrt(-x*x - y*y + 40000)), y / max(1, sqrt(-x*x - y*y + 40000)), 1);
}

Point3D Ripple(int x, int y, int w, int h)
{
	auto x1 = x / 100.0;
	auto y1 = y / 100.0;
	auto r = x1 * x1 + y1 * y1;
	return Point3D(-2 * x1 * cos(r), -2 * y1 * cos(r), 1);
}

Point3D Bumps(int x, int y, int w, int h)
{
	auto x1 = x / 20.0;
	auto y1 = y / 20.0;
	return Point3D(5 * cos(x1) * cos(y1), -5 * sin(x1) * sin(y1), 1);
}