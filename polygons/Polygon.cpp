#include "stdafx.h"
#include "Primitives.h"
#include <vector>
#include <algorithm>

void Poly::AddVertex(const int& x, const int& y)
{
	list.AddVertex(Vertex(x, y));
}

void Poly::MoveLastVertex(const int& x, const int& y)
{
	list.LastVertex()->current->Move(x, y);
}

bool Poly::CanClose(const int& x, const int& y) const
{
	return VertexCount() >= 3 && list.FirstVertex()->current->CheckHit(x, y);
}

void Poly::Close()
{
	list.ClosePolygon();
	if (SignedArea() < 0)
		list.Reverse();
	closed = true;
}

VertexNode* Poly::FindVertex(const int& x, const int& y)
{
	return list.FindVertex(x, y);
}

SegmentNode* Poly::FindSegment(const int& x, const int& y)
{
	return list.FindSegment(x, y);
}

void Poly::RemoveVertex(VertexNode* vertex_node)
{
	list.RemoveVertex(vertex_node);
}

void Poly::Bisect(SegmentNode* segment_node)
{
	list.BisectSegment(segment_node);
}

bool Poly::CheckHit(const int& x, const int& y) const
{
	Vertex start(x, y), end(2000, y);
	Segment ray(start, end);
	auto segment_node = list.FirstSegment();
	auto hits = 0;
	for (auto s = 0; s < list.segment_count; ++s, segment_node = segment_node->next)
		if (segment_node->current->Intersects(ray)) hits++;
	auto vertex_node = list.FirstVertex();
	for (auto v = 0; v < list.vertex_count; ++v, vertex_node = vertex_node->next)
	{
		auto vertex = vertex_node->current;
		if (vertex->x >= x && vertex->y == y) hits--;
	}
	return hits % 2 == 1;
}

void Poly::TranslateVertices(const int& dx, const int& dy)
{
	auto vertex_node = list.FirstVertex();
	for (auto v = 0; v < list.vertex_count; ++v, vertex_node = vertex_node->next)
		vertex_node->current->Translate(dx, dy);
}

void Poly::SetSelection(bool selected)
{
	this->selected = selected;
}

bool Poly::SelfIntersecting() const
{
	auto first_segment = list.FirstSegment();
	for (auto i = 0; i < list.segment_count; ++i, first_segment = first_segment->next)
	{
		auto second_segment = first_segment->next;
		for (auto j = i + 1; j < list.segment_count; ++j, second_segment = second_segment->next)
		{
			if (first_segment->current->Intersects(*second_segment->current) == 1)
			{
				return true;
			}
		}
	}
	return false;
}

std::vector<Poly*> Poly::Intersect(Poly& other)
{
	std::vector<Intersection> intersections;
	std::vector<Intersection> sort_helper;
	std::vector<Poly*> polygons;

	if (this->SelfIntersecting() || other.SelfIntersecting())
	{
		MessageBox(nullptr, L"Nie mo�na wyznaczy� przeci�cia dla wielok�t�w samoprzecinaj�cych si�.", L"B��d", MB_OK);
		return polygons;
	}

	auto this_segment = list.FirstSegment();
	RemoveCollinearPoints(other);
	for (auto i = 0; i < list.segment_count; ++i, this_segment = this_segment->next)
	{
		auto other_segment = other.list.FirstSegment();
		for (auto j = 0; j < other.list.segment_count; ++j, other_segment = other_segment->next)
		{
			if (this_segment->current->Intersects(*other_segment->current) == 1)
			{
				auto intersection = this_segment->current->IntersectionPoint(*other_segment->current);
				auto first_to_second = (this_segment->current->end - this_segment->current->start) * (other_segment->current->end - this_segment->current->start) < 0;
				auto this_vertex = list.BisectSegment(this_segment, intersection);
				auto other_vertex = other.list.BisectSegment(other_segment, intersection);
				intersections.push_back(Intersection(this_vertex, other_vertex, first_to_second));
				this_segment = this_vertex->in_segment;
				other_segment = other_vertex->in_segment;
			}
		}
	}

	auto count = intersections.size();
	if (count == 0)
	{
		auto this_first_vertex = this->list.FirstVertex()->current;
		auto other_first_vertex = other.list.FirstVertex()->current;
		if (this->CheckHit(other_first_vertex->x, other_first_vertex->y))
		{
			polygons.push_back(new Poly(other));
		}
		else if (other.CheckHit(this_first_vertex->x, this_first_vertex->y))
		{
			polygons.push_back(new Poly(*this));
		}
		else
		{
			MessageBox(nullptr, L"Wielok�ty nie przecinaj� si�.", L"B��d", MB_OK);
		}
		return polygons;
	}

	while (true)
	{
		auto first_intersection = intersections.begin();
		for (; first_intersection != intersections.end(); ++first_intersection)
		{
			if (first_intersection->first_to_second && !first_intersection->visited) break;
		}
		if (first_intersection == intersections.end()) break;
		first_intersection->visited = true;
		auto polygon = new Poly();
		auto first_vertex = first_intersection->first;
		auto first_to_second = first_intersection->first_to_second;
		auto next_vertex = first_vertex;
		do
		{
			polygon->AddVertex(next_vertex->current->x, next_vertex->current->y);
			for (auto it = intersections.begin(); it != intersections.end(); ++it)
			{
				if (!it->visited && next_vertex == (first_to_second ? it->first : it->second))
				{
					next_vertex = first_to_second ? it->second : it->first;
					first_to_second = it->first_to_second;
					it->visited = true;
					break;
				}
			}
			next_vertex = next_vertex->next;
		}
		while (next_vertex != first_intersection->second);
		polygon->AddVertex(polygon->list.FirstVertex()->current->x, polygon->list.FirstVertex()->current->y);
		polygon->Close();
		polygons.push_back(polygon);
	}

	return polygons;
}

void Poly::RemoveCollinearPoints(const Poly& other)
{
	auto this_segment = list.FirstSegment(), other_segment = other.list.LastSegment();
	for (auto i = 0; i < list.segment_count; ++i, this_segment = this_segment->next)
	{
		for (auto j = 0; j < other.list.segment_count; ++j, other_segment = other_segment->previous)
		{
			auto val = this_segment->current->Intersects(*other_segment->current);
			if (val == 2)
			{
				this_segment->current->start.x += 1;
			}
		}
	}
}

long Poly::SignedArea() const
{
	long long signed_area = 0;
	auto vit = list.FirstVertex();
	for (auto i = 0; i < list.vertex_count; ++i, vit = vit->next)
		signed_area += (*vit->current) * (*vit->next->current);
	return signed_area;
}

double Point3D::length() const
{
	return sqrt(x * x + y * y + z * z);
}

Point3D Point3D::normalize() const
{
	return *this / this->length();
}

Point3D& Point3D::operator+=(const Point3D& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	return *this;
}

Point3D& Point3D::operator-=(const Point3D& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	return *this;
}

Point3D& Point3D::operator*=(const double& rhs)
{
	x *= rhs;
	y *= rhs;
	z *= rhs;
	return *this;
}

Point3D& Point3D::operator/=(const double& rhs)
{
	x /= rhs;
	y /= rhs;
	z /= rhs;
	return *this;
}