#include "stdafx.h"
#include "Primitives.h"

Vertex::Vertex(const int& x, const int& y)
{
	this->x = x;
	this->y = y;
}

void Vertex::Move(const int& x, const int& y)
{
	this->x = x;
	this->y = y;
}

void Vertex::Translate(const int& dx, const int& dy)
{
	x += dx;
	y += dy;
}

bool Vertex::CheckHit(const int& x, const int& y) const
{
	return abs(this->x - x) <= MARKER_SIZE &&
		abs(this->y - y) <= MARKER_SIZE;
}

Vertex& Vertex::operator+=(const Vertex& rhs)
{
	this->x += rhs.x;
	this->y += rhs.y;
	return *this;
}

Vertex& Vertex::operator-=(const Vertex& rhs)
{
	this->x -= rhs.x;
	this->y -= rhs.y;
	return *this;
}

Vertex& Vertex::operator/=(const int& rhs)
{
	this->x /= rhs;
	this->y /= rhs;
	return *this;
}

Vertex& Vertex::operator=(Vertex rhs)
{
	this->x = rhs.x;
	this->y = rhs.y;
	return *this;
}

long Vertex::operator*(const Vertex& rhs) const
{
	return x * rhs.y - rhs.x * y;
}


Vertex operator+(Vertex lhs, const Vertex& rhs)
{
	lhs += rhs;
	return lhs;
}

Vertex operator-(Vertex lhs, const Vertex& rhs)
{
	lhs -= rhs;
	return lhs;
}

Vertex operator/(Vertex lhs, const int& rhs)
{
	lhs /= rhs;
	return lhs;
}
