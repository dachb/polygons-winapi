﻿#include "stdafx.h"
#include "States.h"
#include "Windowsx.h"
#include "Primitives.h"
#include <vector>

// State manager
// ---------------

StateManager::StateManager(const HWND& hWnd) : hWnd(hWnd)
{
	currentState = new IdleState();
}

StateManager::~StateManager()
{
	delete currentState;
}

void StateManager::ExecuteStateAction(const CanvasData& canvas_data, const UINT& message, const WPARAM& wParam, const LPARAM& lParam) const
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		SetCapture(hWnd);
		currentState->MouseDown(canvas_data, wParam, LEFT_BUTTON, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		break;
	case WM_LBUTTONUP:
		currentState->MouseUp(canvas_data, LEFT_BUTTON, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		ReleaseCapture();
		break;
	case WM_RBUTTONDOWN:
		SetCapture(hWnd);
		currentState->MouseDown(canvas_data, wParam, RIGHT_BUTTON, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		break;
	case WM_RBUTTONUP:
		currentState->MouseUp(canvas_data, RIGHT_BUTTON, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		ReleaseCapture();
		break;
	case WM_MOUSEMOVE:
		currentState->MouseMove(canvas_data, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		break;
	case WM_KEYDOWN:
		currentState->KeyDown(canvas_data, wParam);
		break;
	case WM_KEYUP:
		currentState->KeyUp(canvas_data, wParam);
		break;
	case WM_COMMAND:
		currentState->Command(canvas_data, LOWORD(wParam));
		break;
	default:
		break;
	}
}

void StateManager::HandleEvent(const CanvasData& canvas_data, const UINT& message, const WPARAM& wParam, const LPARAM& lParam)
{
	while (true)
	{
		ExecuteStateAction(canvas_data, message, wParam, lParam);
		if (!currentState->isDone) break;
		auto oldState = currentState;
		currentState = currentState->nextState;
		delete oldState;
	}
}
