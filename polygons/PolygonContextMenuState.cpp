#include "stdafx.h"
#include "States.h"
#include <algorithm>
#include <cassert>

PolygonContextMenuState::PolygonContextMenuState(Poly* polygon)
{
	assert(polygon != nullptr, "Polygon cannot be null");
	this->polygon = polygon;
}

void PolygonContextMenuState::MouseUp(const CanvasData& canvas_data, MouseButton button, const int& x, const int& y)
{
	if (!polygon->CheckHit(x, y)) {
		isDone = true;
		nextState = new IdleState();
		return;
	}
	auto hMenu = CreatePopupMenu();
	POINT point;
	point.x = x;
	point.y = y;
	ClientToScreen(canvas_data.hWnd, &point);
	InsertMenu(hMenu, -1, MF_BYPOSITION | MF_STRING, DELETE_POLYGON, T_DELETE_POLYGON);
	TrackPopupMenu(hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, canvas_data.hWnd, nullptr);
}

void PolygonContextMenuState::Command(const CanvasData& canvas_data, const int& command)
{
	auto& polygons = canvas_data.polygons;
	switch (command)
	{
	case DELETE_POLYGON:
		polygons.erase(remove(polygons.begin(), polygons.end(), polygon), polygons.end());
		delete polygon;
		break;
	default:
		break;
	}
	isDone = true;
	nextState = new IdleState();
}
